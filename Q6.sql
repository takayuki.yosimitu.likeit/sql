/* 問６. 以下のようなデータを取得するSQLを作成してください。

・「item」テーブルから「item_price」が  1000  以上の列のみ抽出する
・ 取得するカラムは「item_name」と「 item_price 」のみ */

SELECT item_name, item_price
FROM item
WHERE item_price > 1000;
