/* 問２.  以下のテーブルを作成するDDLを作成してください
テーブル名：item
物理名.         データ型.            属性
item_id.        INT.               NOT NULL, AUTO_INCREMENT,PRIMARY KEY
item_name.      VARCHAR(256).      NOT NULL
Item_price      INT.               NOT NULL
category_id     INT
 */
CREATE TABLE item(
item_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
item_name VARCHAR(256) NOT NULL,
Item_price INT NOT NULL,
category_id INT
);
