/* 問５. 以下のようなデータを取得するSQLを作成してください。

・「item」テーブルから「category_id」が  1  の列のみ抽出する
・ 取得するカラムは「item_name」と「item_price」のみ */

SELECT item_name, item_price
FROM item
WHERE category_id = 1;
