/* 問７. 以下のようなデータを取得するSQLを作成してください。

・「item」テーブルから「item_name」に「肉」を含む列を抽出する
・ 取得するカラムは「item_name」と「 item_price 」のみ */

SELECT item_name, item_price
FROM item
WHERE item_name LIKE "%肉%"
